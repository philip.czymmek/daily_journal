# Daily journal program
# Save daily journal entries inside a txt file as an archive
# Step 1: Print welcome screen
# Step 2: Select new entry
# Step 3: Add new text into the field
# Step 4: Save data into text file

from datetime import datetime

# get list of 3 items I am grateful and I want to achieve for
def generateList(topic):
    list = []
    for statement in range(3):
       statement = input(topic)
       list.append(topic + statement + '\n')
    print(list)
    writeToFile(file, list)    


# prepare file for text entry
# 1. Open file 
# 2. Get lines to write
# 3. Flush output buffer
# 4. Close file
def writeToFile(file, text):
    with open(file, 'a') as f:
        f.writelines(text)
        f.flush()

file = 'journal.txt'
journal = 'Journal entry '
Topics = ('I am grateful for ', 'I want to achieve ')
greeting = '''###################################################################
##              Welcome to the daily journal system              ##
###################################################################'''


print(greeting)

# get time and date
now = datetime.now()

# format the datetime to dd/mm/yy HH/MM/SS
now = now.strftime('%d/%m/%Y %H:%M:%S\n')
print('Today is the: ', now)

writeToFile(file, journal + now)

for topic in Topics: 
    generateList(topic) 


